extends KinematicBody2D
const GRABITATEA = 10
export (int) var speed = 200

var norantza=1
var velocity = Vector2()

func hil():
	queue_free()

func _physics_process(delta):
	velocity.x = speed*norantza
	velocity.y += GRABITATEA
	$AnimatedSprite.play("ibili")
	move_and_slide(velocity, Vector2( 0, -1 ))
	if is_on_wall():
		norantza = norantza* -1
	if norantza==1:
		$AnimatedSprite.flip_h = false
	else:
		$AnimatedSprite.flip_h = true
	

	
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_Area2D_body_entered(body):
	if "Jokalaria" in body.name:
		body.mina_jaso()
	pass # replace with function body

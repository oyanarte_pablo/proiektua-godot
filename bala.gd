extends Area2D

const SPEED = 350
var velocity = Vector2()
var norantza = 1

func set_norantza(nor):
	norantza = nor
	if norantza == 1:
		$AnimatedSprite.flip_h = false
	else:
        $AnimatedSprite.flip_h = true

func _physics_process(delta):
	velocity.x = SPEED * delta * norantza
	translate(velocity)
	$AnimatedSprite.play("default")
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_bala_body_entered(body):
	if "etsaia" in body.name:
		body.hil()
		queue_free()
